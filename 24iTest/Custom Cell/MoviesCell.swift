//
//  MoviesCell.swift
//  24iTest
//
//  Created by Rockville on 03/05/2019.
//  Copyright © 2019 Usman. All rights reserved.
//

import UIKit

class MoviesCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgPoster: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func displayContent(data:MoviesResult) {
        self.lblTitle.text = data.title
        if let posterPath = data.poster_path{
            let imgStr = kBaseImageUrl + posterPath
            let url = URL(string: imgStr )
            self.imgPoster.af_setImage(withURL: url!, placeholderImage: #imageLiteral(resourceName: "placeholder.png"))
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
