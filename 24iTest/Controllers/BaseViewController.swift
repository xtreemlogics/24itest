//
//  BaseViewController.swift
//  24iTest
//
//  Created by Rockville on 03/05/2019.
//  Copyright © 2019 Usman. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BaseViewController: UIViewController,NVActivityIndicatorViewable {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        self.hideKeyboardWhenTappedAround()
    }

    func startLoading()  {
        let size = CGSize(width: 50 , height:50)
    
        startAnimating(size, message: "", messageFont: UIFont.boldSystemFont(ofSize: 19), type: NVActivityIndicatorType(rawValue: 23), color: UIColor.white, padding: CGFloat(1.0), textColor: UIColor.red)
        
    }
    func stopLoading()  {
        stopAnimating()
        
    }
    func showAlertWith(message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
    }
}
