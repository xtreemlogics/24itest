//
//  MovieDetailViewController.swift
//  24iTest
//
//  Created by Rockville on 03/05/2019.
//  Copyright © 2019 Usman. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import XCDYouTubeKit
class MovieDetailViewController: BaseViewController {

    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblgenres: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblOverview: UILabel!
    var movieId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Movie Detail"
        getMovieDetail()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnWatchTrailerPress(_ sender: Any) {
        getMovieVideo()
    }
    func playVideo(video:Results) {
        
        let videoPlayerViewController = XCDYouTubeVideoPlayerViewController(videoIdentifier: video.key)
        NotificationCenter.default.addObserver(self, selector: #selector(self.moviePlayerPlaybackDidFinish(_:)), name: .MPMoviePlayerPlaybackDidFinish, object: videoPlayerViewController.moviePlayer)
        presentMoviePlayerViewControllerAnimated(videoPlayerViewController)
    }
    
    @objc func moviePlayerPlaybackDidFinish(_ notification: Notification?) {
        NotificationCenter.default.removeObserver(self, name: .MPMoviePlayerPlaybackDidFinish, object: notification?.object)
        let finishReason = MPMovieFinishReason(rawValue: (notification?.userInfo?[MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] as AnyObject).intValue ?? 0)
        if finishReason == .playbackError {
            _ = notification?.userInfo?[XCDMoviePlayerPlaybackDidFinishErrorUserInfoKey] as? Error
            // Handle error
        }
    }
   
    func displayData(data:MovieDetail) {
        if let posterPath = data.poster_path{
            let imgStr = kBaseImageUrl + posterPath
            let url = URL(string: imgStr )
            self.imgBanner.af_setImage(withURL: url!, placeholderImage:#imageLiteral(resourceName: "placeholder.png"))
        }
        lblTitle.text = data.title
        let arrNames = data.genres.map({$0.name})
    
        
        lblgenres.text = arrNames.joined(separator: ", ")
        lblDate.text = Utility.formattedDateOnly(date: data.release_date)
        lblOverview.text = data.overview
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


// Mark API calls
extension MovieDetailViewController{
    // MARK API call for movie detail
    func getMovieDetail(){
        if (!(NetworkReachabilityManager()?.isReachable)!) {
            self.showAlertWith(message: ApiErrorMessage.NoNetwork)
            return
        }
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            self.stopLoading()
            
            do {
                // Decode data to object
                let jsonData = Utility.json(from: result)
                let jsonDecoder = JSONDecoder()
                
                
                let data = try jsonDecoder.decode(MovieDetail.self, from: (jsonData?.data(using: .utf8)!)!)
                self.displayData(data: data)
               
                
            }
            catch {
                print("Json Error: " + error.localizedDescription)
                
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {_ in
             self.stopLoading()
             self.showAlertWith(message: ApiErrorMessage.ErrorOccured)
            
        }
        
        self.startLoading()
        
        let params:Parameters = ["api_key":"aea6909230a38c15259ca7732d818b9e"
        ]
        APIManager.sharedInstance.authenticationManagerAPI.getMovieDetail(parameters: params, movieId: movieId, type: HTTPMethod.get, success: successClosure, failure: failureClosure)
    }
    
    
    func getMovieVideo(){
        if (!(NetworkReachabilityManager()?.isReachable)!) {
            self.showAlertWith(message: ApiErrorMessage.NoNetwork)
            return
        }
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            self.stopLoading()
            
            do {
                // Decode data to object
                let jsonData = Utility.json(from: result)
                let jsonDecoder = JSONDecoder()
                
                
                let data = try jsonDecoder.decode(YoutubeVideoUrl.self, from: (jsonData?.data(using: .utf8)!)!)
                self.playVideo(video: data.results[0])
            }
            catch {
                print("Json Error: " + error.localizedDescription)
                
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {_ in
            self.stopLoading()
            self.showAlertWith(message: ApiErrorMessage.ErrorOccured)
            
        }
        
        self.startLoading()
        
        let params:Parameters = ["api_key":"aea6909230a38c15259ca7732d818b9e"
        ]
        APIManager.sharedInstance.authenticationManagerAPI.getMovieVideos(parameters: params, movieId: movieId, type: HTTPMethod.get, success: successClosure, failure: failureClosure)
    }
}
