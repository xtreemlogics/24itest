//
//  ViewController.swift
//  24iTest
//
//  Created by Rockville on 03/05/2019.
//  Copyright © 2019 Usman. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
class ViewController: BaseViewController {

    @IBOutlet weak var searchBarBottom: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var moviesTableView: UITableView!
    var moviesList = MovieCatalog()
    var currentMoviesList = [MoviesResult]()
    private var PAGE = 0
    private var selectedMovieId = 0
    private var IS_LOAD_MORE = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Movie Catalog"
        searchBar.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShown), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        self.getPopularMovies()
        // Do any additional setup after loading the view.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toMovieDetail"{
            let vc = segue.destination as! MovieDetailViewController
            vc.movieId = selectedMovieId
        }
    }
    
    // Move searchbar on keyboard appears
    @objc func keyboardWillShown(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: {() -> Void in
            self.searchBarBottom.constant = keyboardFrame.size.height 
        })
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: {() -> Void in
            self.searchBarBottom.constant = 0
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
//MARK:- Tableview DataSource
extension ViewController : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentMoviesList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! MoviesCell
        let data = self.currentMoviesList[indexPath.row]
        cell.displayContent(data: data )
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
//MARK:- Tableview Delegate
extension ViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movieData = self.currentMoviesList[indexPath.row]
        self.selectedMovieId = movieData.id 
        self.performSegue(withIdentifier: "toMovieDetail", sender: self)
    }
}

// Mark:- Search Bar delegate
extension ViewController:UISearchBarDelegate{

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            currentMoviesList = moviesList.results
            moviesTableView.reloadData()
            return
        }
        currentMoviesList = moviesList.results.filter({movie -> Bool in
            
            movie.title.lowercased().contains(searchText.lowercased())
        })
        
        moviesTableView.reloadData()
        
    }
}
// uncomment to load next page items on scrolling last item
/*
extension ViewController: UIScrollViewDelegate {
 
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
         if(self.moviesTableView.contentOffset.y >= (self.moviesTableView.contentSize.height - self.moviesTableView.bounds.size.height)) {
            if self.IS_LOAD_MORE && self.PAGE != 0{
                self.getPopularMovies()
 
            }
        }
    }
}
  */

// Mark API calls
extension ViewController{
    // MARK API call for Popular movies
    func getPopularMovies(){
        if (!(NetworkReachabilityManager()?.isReachable)!) {
            self.showAlertWith(message: ApiErrorMessage.NoNetwork)
            return
        }
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            self.stopLoading()
            
            do {
                // Decode data to object
                let jsonData = Utility.json(from: result)
                let jsonDecoder = JSONDecoder()
                
                self.moviesList = MovieCatalog()
                self.currentMoviesList = [MoviesResult]()
                self.moviesList = try jsonDecoder.decode(MovieCatalog.self, from: (jsonData?.data(using: .utf8)!)!)
                self.currentMoviesList = self.moviesList.results
                self.moviesTableView.reloadData()
                // uncomment for loading next pages on scrolling
                /*let data = try jsonDecoder.decode(MovieCatalog.self, from: (jsonData?.data(using: .utf8)!)!)
                if (data.page ?? 0 <= data.total_pages ?? 0){
                    self.PAGE = data.page ?? 0
                    self.moviesList.results = self.moviesList.results! + data.results!
                    self.moviesTableView.reloadData()
                    self.IS_LOAD_MORE = true
                }
                else {
                    self.IS_LOAD_MORE = false
                }*/
            }
            catch {
                print("Json Error: " + error.localizedDescription)
                //   self.showAlertWith(message: result["message"] as! String)
               
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {_ in
            self.stopLoading()
            self.showAlertWith(message: ApiErrorMessage.ErrorOccured)
           
        }
        
        self.startLoading()
        self.PAGE += 1
        let params:Parameters = ["api_key":"aea6909230a38c15259ca7732d818b9e",
                                 "page": self.PAGE
                                 ]
        APIManager.sharedInstance.authenticationManagerAPI.getPopularMovies(parameters: params, type:HTTPMethod.get , success: successClosure, failure: failureClosure)

    }
}
