//
//  MovieDetail.swift
//  24iTest
//
//  Created by Rockville on 03/05/2019.
//  Copyright © 2019 Usman. All rights reserved.
//

import Foundation


final class MovieDetail: Codable {
    var adult: Bool = false
    var backdrop_path: String? = ""
    var belongs_to_collection: BelongToCollection? = BelongToCollection()
    var budget: Int = 0
    var genres: [Genre] = [Genre]()
    var homepage: String? = ""
    var id: Int = 0
    var imdb_id:String = ""
    var original_language, original_title, overview: String 
    var popularity: Double = 0
    var poster_path: String? = ""
    var production_companies: [ProductionCompany] = [ProductionCompany]()
    var production_countries: [ProductionCountry] = [ProductionCountry]()
    var release_date: String = ""
    var revenue, runtime: Int 
    var spoken_languages: [SpokenLanguage] = [SpokenLanguage]()
    var status, tagline, title: String
    var video: Bool = false
    var vote_average: Double = 0
    var vote_count: Int = 0
   
}

final class Genre: Codable {
    var id: Int = 0
    var name: String = ""
    
 
}

final class ProductionCompany: Codable {
    var id: Int = 0
    var logo_path: String? = ""
    var name, origin_country: String
    
    
    
}

final class ProductionCountry: Codable {
    var iso_3166_1, name: String
    
}

final class SpokenLanguage: Codable {
    var iso_639_1, name: String
    
}

final class BelongToCollection:Codable{
    var id: Int = 0
    var name: String = ""
    var poster_path: String = ""
    var backdrop_path:String = ""
}

