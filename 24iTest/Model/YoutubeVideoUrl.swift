//
//  YoutubeVideoUrl.swift
//  24iTest
//
//  Created by Rockville on 03/05/2019.
//  Copyright © 2019 Usman. All rights reserved.
//

import Foundation

final class YoutubeVideoUrl:Codable{
    var id:Int = 0
    var results:[Results] = [Results]()
}
final class Results:Codable{
 
    var id:String = ""
    var iso_639_1:String = ""
    var iso_3166_1:String = ""
    var key:String = ""
    var name:String = ""
    var site:String = ""
    var size:Int = 0
    var type:String = ""
}
