//
//  MovieCatalog.swift
//  24iTest
//
//  Created by Rockville on 03/05/2019.
//  Copyright © 2019 Usman. All rights reserved.
//

import Foundation

final class MovieCatalog: Codable{
    
    var page : Int = 0
    var results : [MoviesResult] = [MoviesResult]()
    var total_results: Int = 0
    var total_pages : Int = 0
    
}
final class MoviesResult:Codable{
    var poster_path : String? = ""
    var adult: Bool = false
    var overview : String = ""
    var release_date : String = ""
    var genre_ids: [Int] = []
    var id: Int = 0
    var original_title : String = ""
    var original_language : String = ""
    var title : String = ""
    var backdrop_path : String? = ""
    var popularity: Double = 0
    var vote_count: Int = 0
    var video: Bool = false
    var vote_average: Double = 0
}

