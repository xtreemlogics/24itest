
import UIKit


typealias DefaultAPIFailureClosure = (NSError) -> Void
typealias DefaultAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void
typealias DefaultBoolResultAPISuccesClosure = (Bool) -> Void
typealias DefaultArrayResultAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void


protocol APIErrorHandler {
    func handleErrorFromResponse(response: Dictionary<String,AnyObject>)
    func handleErrorFromERror(error:NSError)
}


final class APIManager: NSObject {
    static let sharedInstance = APIManager(token: "")
    var serverToken: String?
    private init(token: String) {
        self.serverToken = token
    }
    let authenticationManagerAPI = AuthenticationAPIManager()
}



