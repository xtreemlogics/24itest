

import UIKit
import Alamofire

class AuthenticationAPIManager: APIManagerBase {
    
    // Popular movies
    
    func getPopularMovies(parameters:Parameters,
                       type:HTTPMethod,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.Popular.rawValue)!
        self.serverRequestWithParams(route: route,encoding:URLEncoding(destination:.queryString), header: [:], params: parameters, requestType: type, success: success, failure: failure)
    }
    
    //  movie detail
    func getMovieDetail(parameters:Parameters,
                          movieId:Int,
                          type:HTTPMethod,
                          success:@escaping DefaultArrayResultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: "\(movieId)")!
        self.serverRequestWithParams(route: route,encoding:URLEncoding(destination:.queryString), header: [:], params: parameters, requestType: type, success: success, failure: failure)
    }
    
    //  movie video
    func getMovieVideos(parameters:Parameters,
                        movieId:Int,
                        type:HTTPMethod,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        let routeStr = "\(movieId)" + Route.videos.rawValue
        let route: URL = POSTURLforRoute(route: routeStr)!
        self.serverRequestWithParams(route: route,encoding:URLEncoding(destination:.queryString), header: [:], params: parameters, requestType: type, success: success, failure: failure)
    }
    
}
