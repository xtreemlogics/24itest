//
//  Strings.swift
//  24iTest
//
//  Created by Rockville on 03/05/2019.
//  Copyright © 2019 Usman. All rights reserved.
//

import Foundation
struct ApiErrorMessage {
    
    static let NoNetwork = "No Network"
    
    static let ErrorOccured = "An error occurred. Please try again."
}
