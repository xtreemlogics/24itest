//
//  Utility.swift
//  24iTest
//
//  Created by Rockville on 03/05/2019.
//  Copyright © 2019 Usman. All rights reserved.
//

import Foundation
import UIKit


final class Utility : NSObject {
    
    
    final class func emptyTableViewMessage(message:String, viewController: UIViewController, tableView:UITableView) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: viewController.view.bounds.size.width, height: viewController.view.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.boldSystemFont(ofSize: 10)
        messageLabel.sizeToFit()
        
        tableView.backgroundView = messageLabel
        tableView.separatorStyle = .none
    }
    

    final class func getScreenWidth() -> CGFloat {
        return UIScreen.main.bounds.width
    }
    
    final class func getScreenHeight() -> CGFloat {
        return UIScreen.main.bounds.height
    }
    
    
    
    final class func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    final class func formattedDateOnly(date:String)->String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        let dateRelease = dateFormatter.date(from:date)!
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: dateRelease)
    }
    
}
